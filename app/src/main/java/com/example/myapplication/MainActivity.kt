package com.example.myapplication

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var countCorrect = 0
        var countIncorrect = 0
        var a =0
        var b =0
        Game(countCorrect,countIncorrect)





    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun Game(a: Int, b: Int) {
        var num1 = Random.nextInt(1,9)
        var num2 = Random.nextInt(1,9)
        var result = num1+num2
        var text1: TextView = findViewById(R.id.TV1) as TextView
        text1.setText("${num1}")
        var text2: TextView = findViewById(R.id.TV2) as TextView
        text2.setText("${num2}")
        var btn1: Button = findViewById(R.id.btn_result1) as Button
        var btn2: Button = findViewById(R.id.btn_result2) as Button
        var btn3: Button = findViewById(R.id.btn_result3) as Button
        var countCorrect = a
        var countIncorrect = b
        var countCorrectText: TextView = findViewById(R.id.countCorrect) as TextView
        var countIncorrectText: TextView = findViewById(R.id.countIncorrect) as TextView
        var TV3: TextView = findViewById(R.id.TV3) as TextView



        var position = Random.nextInt(1,3)
        if (position==1){
            btn1.setText("${result}")
            btn2.setText("${result+1}")
            btn3.setText("${result+2}")
        }
        if (position==2) {
            btn1.setText("${result-1}")
            btn2.setText("${result}")
            btn3.setText("${result+1}")
        }
        if (position==3) {
            btn1.setText("${result-2}")
            btn2.setText("${result-1}")
            btn3.setText("${result}")
        }

        btn1.setOnClickListener{
            var getTextbtn = btn1.getText()
            if ("${result}"=="${getTextbtn}"){
                TV3.visibility = View.VISIBLE
                TV3.setText("Correct")
                TV3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                Game(countCorrect, countIncorrect)
            }else {
                TV3.visibility = View.VISIBLE
                TV3.setText("Incorrect")
                TV3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
                countIncorrect++
                countIncorrectText.setText("${countIncorrect}")
            }
        }
        btn2.setOnClickListener{
            var getTextbtn = btn2.getText()
            if ("${result}"=="${getTextbtn}"){
                TV3.visibility = View.VISIBLE
                TV3.setText("Correct")
                TV3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                Game(countCorrect, countIncorrect)
            }else {
                countIncorrect++
                countIncorrectText.setText("${countIncorrect}")
                TV3.visibility = View.VISIBLE
                TV3.setText("Incorrect")
                TV3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
                }
        }
        btn3.setOnClickListener{
            var getTextbtn = btn3.getText()
            if ("${result}"=="${getTextbtn}"){
                TV3.visibility = View.VISIBLE
                TV3.setText("Correct")
                TV3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
                countCorrect++
                countCorrectText.setText("${countCorrect}")
                Game(countCorrect, countIncorrect)
            }else {
                countIncorrect++
                countIncorrectText.setText("${countIncorrect}")
                TV3.visibility = View.VISIBLE
                TV3.setText("Incorrect")
                TV3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
            }
        }
    }

}